var express = require('express');

var router = express.Router();
var formidable = require('formidable');
var cmd=require('node-cmd');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
// POST: handle form data sent from client
router.post('/upload', function(req, res) {


	var form = new formidable.IncomingForm();
	var index, filename;

	form.parse(req);

	form.on('field', function(name, value) {
		if (name == 'index') index = value;
		console.log('value',value)
	});

	form.on('fileBegin', function(name, file) {
		file.path = './uploads/' + file.name;
	});
	
	form.on('file', function(name, file) {
		filename = file.name;
	});

	form.on('end', function() {
		cmd.get(
	        'ipfs add '+'./uploads/' + filename,
	        function(err, data, stderr){
	        	res.send({
					index: index,
					filename: filename,
					hash:data.split(' ')[1]
				});
	        	console.log("lo que voto fue:",stderr);
	            console.log('the current working dir is : ',data)
	        }
	    );

	});

	form.on('error', function () {
		res.end('Something went wrong on ther server side. Your file may not have yet uploaded.');
	});
});

module.exports = router;
